# Field Show More

Field Show More module allows you to control how many field values are displayed when there are multiple values. It provides a "Show more" link that will display the remaining values. After the "Show more" link is clicked, it will change to "Show less" to hide the remaining values.

## Getting started

1. Install Field Show More in the usual way [Install Modules](https://www.drupal.org/docs/extending-drupal/installing-modules)
2. Go to the display settings for the multi-value field on the entity whose display you like to change.
3. In the "Show more" settings, choose the desired number of items to display before the "Show more" link appears.
5. Update the field formatter settings.
6. Click "Save" for the entity display settings.

## Links

- [Project page](https://gitlab.com/born-digital-public/field_show_more)
- [Submit bug reports, feature suggestions](https://gitlab.com/born-digital-public/field_show_more/-/issues)

## Maintainers

- pdcarto - https://drupal.org/u/pdcarto

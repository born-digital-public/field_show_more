(function (Drupal, $, once) {
  Drupal.behaviors.field_show_more = {
    attach: function (context, settings) {
      $(once('fieldShowMoreLess', '[field-show-more-id]', context)).each(function () {
        var $fieldContainer = $(this);
        var $hiddenItems = $fieldContainer.find('.field-show-more-less');
        if($hiddenItems.length > 0) {
          var showMoreText = 'Show ' + $hiddenItems.length  +'  more';
          var showLessText = 'Show ' + $hiddenItems.length  +'  less';

          var $showMoreLink = $('<a href="#" class="show-more-link">' + showMoreText + '</a>');

          // Append the "Show more" link at the end of the field container.
          $fieldContainer.append($showMoreLink);
          // Add click event to the "Show more" link
          $showMoreLink.click(function (e) {
            e.preventDefault();
            var $link = $(this);

            if ($link.text() === showMoreText) {
              $hiddenItems.show();
              $link.text(showLessText);
            } else {
              $hiddenItems.hide();
              $link.text(showMoreText);
            }
          });
        }
      });

    }
  };
})(Drupal, jQuery, once);
